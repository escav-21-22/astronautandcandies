using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script para gestionar la recolecci�n del coleccionable
/// </summary>
public class CollectableController : MonoBehaviour
{    

    // Se ejecuta cuando el objeto entra en colisi�n con otro.
    // Es necesario que ambos tengan Collider asignado, sin marcar como trigger, y al menos uno de los dos debe tener Rigidbody
    // Tambi�n es importante que todos los elementos y funciones hagan referencia a si el juego es 2D o 3D
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Se destruye el objeto que tenga el script
        Destroy(gameObject);
    }

}
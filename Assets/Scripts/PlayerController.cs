using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Se necesita incluir para utilizar elementos de interfaz (Text en este caso)
using UnityEngine.UI;

/// <summary>
/// Script para gestionar el movimiento del jugador
/// </summary>
public class PlayerController : MonoBehaviour
{
    // Velocidad de movimiento del personaje en el eje X
    public float xMovement;
    // Velocidad de movimiento del personaje en el eje Y
    public float yMovement;

    // Referencia al componente Text donde se muestra el contador de puntos obtenidos
    public Text pointsText;

    // Variable privada con los puntos conseguidos por el jugador
    private int points;

    // Se ejecuta durante el primer frame que el objeto est� activo
    void Start()
    {
        // Inicializaci�n de la variable
        points = 0;
    }

    // Se ejecuta en cada uno de los frames 
    void Update()
    {
        // Input.GetKeyDown devuelve "true" si justo en este frame el jugador ha empezado a pulsar la tecla indicada
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Empiezo a pulsar A");
        }

        // Input.GetKeyUp devuelve "true" si justo en este frame el jugador ha dejado de pulsar la tecla indicada
        if (Input.GetKeyUp(KeyCode.A))
        {
            Debug.Log("Termino de pulsar A");
        }

        // Input.GetKey devuelve "true" mientras que el jugador pulsa la tecla indicada
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Pulsando A");
        }

        // Comprueba si el jugador est� pulsando la tecla A
        if (Input.GetKey(KeyCode.A))
        {
            // Movimiento del jugador usando el Transform
            //transform.Translate(-xMovement, 0, 0);

            // Movimiento del jugador usando f�sicas
            GetComponent<Rigidbody2D>().velocity = Vector2.left * xMovement;
        }

        // Comprueba si el jugador est� pulsando la tecla D
        if (Input.GetKey(KeyCode.D))
        {
            // Movimiento del jugador usando el Transform
            //transform.Translate(xMovement, 0, 0);

            // Movimiento del jugador usando f�sicas
            GetComponent<Rigidbody2D>().velocity = Vector2.right * xMovement;
        }

        // Comprueba si el jugador est� pulsando la tecla W
        if (Input.GetKey(KeyCode.W))
        {
            // Movimiento del jugador usando el Transform
            //transform.Translate(0, yMovement, 0);

            // Movimiento del jugador usando f�sicas
            GetComponent<Rigidbody2D>().velocity = Vector2.up * yMovement;
        }

        // Comprueba si el jugador est� pulsando la tecla S
        if (Input.GetKey(KeyCode.S))
        {
            // Movimiento del jugador usando el Transform
            //transform.Translate(0, -yMovement, 0);

            // Movimiento del jugador usando f�sicas
            GetComponent<Rigidbody2D>().velocity = Vector2.down * yMovement;
        }

        // Comprueba si el jugador no est� pulsando ninguna tecla
        if (!Input.anyKey)
        {
            // Detiene el movimiento por f�sicas del personaje
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Comprueba si el objeto con el que hemos colisionado tiene la etiqueta "Collectable"
        if (collision.gameObject.CompareTag("Collectable"))
        {
            // Incremento de la puntuaci�n
            points += 1;
            // points = points + 1; (forma alternativa)

            // Muestra la puntuaci�n por pantalla, asign�ndola al campo text del componente
            pointsText.text = points.ToString();
            // Muestra la puntuaci�n por consola
            Debug.Log(points);
        }
    }
}
